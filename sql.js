const mysql = require('mysql2');

// Create the connection to database
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'testormnew',
  password: '00000000'
});

// A simple SELECT query
connection.query(
  'SELECT * FROM `users` WHERE `age` > 18',
  function (err, results, fields) {
    console.table(results); // results contains rows returned by server
    console.log(fields); // fields contains extra meta data about results, if available
  }
);

// Using placeholders
connection.query(
  'SELECT * FROM `users` WHERE `email` LIKE "%?%" AND `age` > ?',
  ['example.com', 18],
  function (err, results) {
    console.log(err, results);
  }
);