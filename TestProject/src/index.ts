import { AppDataSource } from "./data-source"
import { User } from "./entity/User"
import { Post } from "./entity/Post"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const user = new User()
    user.firstName = "Timber"
    user.lastName = "Saw"
    user.age = 25
    await AppDataSource.manager.save(user)
    console.log("Saved a new user with id: " + user.id)

    console.log("Loading users from the database...")
    const users = await AppDataSource.manager.find(User)
    console.log("Loaded users: ", users)

    console.log("Here you can setup and run express / fastify / any other framework.")

    console.log("Inserting a new post into the database...")
    const post = new Post()
    post.title = "Hello Post"
    post.content = "This is my first post"
    post.author = user

    await AppDataSource.manager.save(post)
    console.log("Saved a new post with id: " + post.id)

    console.log("Loading posts from the database...")
    const posts = await AppDataSource.manager.find(Post)

    console.log("Loaded posts: ", posts)

}).catch(error => console.log(error))
