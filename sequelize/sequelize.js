const { Sequelize } = require('sequelize')

// connection to mysql
const LOGIN = 'root'
const DB_NAME = 'testormnew'
const PASSWORD = '00000000'

const sequelize = new Sequelize(DB_NAME, LOGIN, PASSWORD, {
    dialect: 'mysql',
    host: 'localhost',
    port: 3306
})

module.exports = {
    sequelize
}