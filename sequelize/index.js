// // const { sequelize } = require('./sequelize')
// const { Sequelize } = require('sequelize')
// const { User } = require('./entities/user')
// // const { Post } = require('./entities/post')
// const { fakeUsers } = require('./fake-data')

// const main = async () => {
//     try {
//         await sequelize.authenticate();
//         console.log('Connection has been established successfully.');
//     } catch (error) {
//         console.error('Unable to connect to the database:', error);
//     }

//     await sequelize.sync({ force: true })

//     for (const user of fakeUsers) {
//         const newUser = new User()

//         newUser.firstName = user.firstName
//         newUser.lastName = user.lastName
//         newUser.age = user.age
//         newUser.role = user.role
//         newUser.email = user.email
//         newUser.city = user.city

//         await newUser.save()
//     }

//     // find All


//     const getUsers = async (where) => {
//         const queryParams = {}

//         if (where) {
//             queryParams.where = where
//         }

//         const users = await User.findAll(queryParams)

//         return JSON.parse(JSON.stringify(users))
//     }

//     // console.table(await getUsers())

//     // update users last name by role

//     const updatedUsers = await User.update(
//         { lastName: "Pupa" },
//         { where: { role: "admin" } }
//     )

//     // console.log("Updated users: ", updatedUsers)
//     // console.log("Users after update", await getUsers())

//     const customersFromNewYork = await getUsers({
//         city: "New-York",
//         role: "customer",
//         age: {
//             [Sequelize.Op.gte]: 18
//         }
//     })

//     console.table(customersFromNewYork)

//     sequelize.close()
// }

// main()

const users = [
    {
        id: 1,
        name: "Artem",
        city: "Kyiv"
    },
    {
        id: 2,
        name: "Alex",
        city: "Odesa"
    },
    {
        id: 3,
        name: "Dima",
        city: "Kyiv"
    },
    {
        id: 4,
        name: "Dima",
        city: "Odesa"
    },
    {
        id: 5,
        name: "Artem",
        city: "Lviv"
    }
]

const indexes = {
    city: {
        Kyiv: [1, 3],
        Odesa: [2, 4],
        Lviv: [5]
    },
    name: {
        Artem: [1, 5],
        Alex: [2],
        Dima: [3, 4]
    }
}

const searchQuery = "Kyiv"
const searchParam = "city"

const searchResult = []

for (const user of users) {
    if (searchParam === "city") {
        const usersIds = indexes[searchParam][searchQuery]

        if (usersIds.includes(user.id)) {
            searchResult.push(user)
        }
    }

    if (searchParam === "name") {
        const usersIds = indexes[searchParam][searchQuery]

        if (usersIds.includes(user.id)) {
            searchResult.push(user)
        }
    }
}

console.table(searchResult)
