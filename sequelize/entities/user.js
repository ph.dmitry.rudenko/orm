const { Sequelize } = require('sequelize')
const { sequelize } = require('../sequelize')

const User = sequelize.define('users', {
    firstName: {
        type: Sequelize.STRING
    },
    lastName: {
        type: Sequelize.STRING
    },
    age: {
        type: Sequelize.INTEGER
    },
    role: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    city: {
        type: Sequelize.STRING
    }
})

module.exports = {
    User
}