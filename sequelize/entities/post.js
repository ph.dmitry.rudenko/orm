const { Sequelize } = require('sequelize')
const { sequelize } = require('../sequelize')
const { User } = require('./user')

const Post = sequelize.define('posts', {
    title: {
        type: Sequelize.STRING
    },
    content: {
        type: Sequelize.STRING
    },
    // author: {
    //     type: User
    // }
})

module.exports = {
    Post
}