const fakeUsers = [
    {
        firstName: "Timber",
        lastName: "Saw",
        age: 25,
        role: "admin",
        email: "5OqFP@example.com",
        city: "London"
    },
    {
        firstName: "Alex",
        lastName: "Saw",
        age: 24,
        role: "customer",
        email: "5OqFP@gmail.com",
        city: "London"
    },
    {
        firstName: "John",
        lastName: "Doe",
        age: 22,
        role: "customer",
        email: "aaaa@example.com",
        city: "New-York"
    },
    {
        firstName: "Mykola",
        lastName: "Simpson",
        age: 16,
        role: "customer",
        email: "bbbb@example.com",
        city: "New-York"
    },
    {
        firstName: "Mykyta",
        lastName: "R",
        age: 24,
        role: "admin",
        email: "admin@admin.com",
        city: "Bangladeh"
    }
]

module.exports = {
    fakeUsers
}